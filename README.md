### TV LOGO DETECT 

TV logo detection with using template matching of opencv.
Detect from live video frame.

```angular2html
 cv2.matchTemplate(gray, logo, cv2.TM_CCOEFF_NORMED)
```

OpenCV 3.2.0

Anaconda 3

python 3.5
